#!/usr/bin/env python3
# coding=utf-8

class Grafo:
    def __init__(self, arq):
        """Construtor da classe grafo"""
        
        fl = open(arq, "r")
        lines = fl.read().splitlines()
        fl.close()
        
        self.nVertices = int(lines[0])
        
        self.vertices = []        
        for i in range(1, self.nVertices+1):
            _v = lines[i].split()
            _v[0] = int(_v[0])
            _v[1] = float(_v[1])
            _v[2] = float(_v[2])
            _v[3] = int(_v[3])
            _v[4] = int(_v[4])
            _v[5] = int(_v[5])
            
            v = {"id":_v[0], "x":_v[1], "y":_v[2], "c":_v[3],
                 "d":_v[4], "w":_v[5], "regiao":0}
                 
            self.vertices.append(v)
        
        self.nArestas = int(lines[self.nVertices+1])
        
        self.arestas = []
        for i in range(self.nVertices+2, self.nVertices+self.nArestas+2):
            _a = lines[i].split()
            _a[0] = int(_a[0])
            _a[1] = int(_a[1])
            
            self.arestas.append(tuple(_a))
        
        config = lines[-1].split()
        self.nRegioes = int(config[0])
        self.lambdaC = float(config[1])
        self.lambdaD = float(config[2])
        self.lambdaW = float(config[3])
        
        self.gerarMA()
        
        self.regioes = {}
        for i in range(self.nVertices):
            r = {"vertices":[i]}
            self.regioes[i+1] = [i]
            self.vertices[i]["regiao"] = i+1
            
        self.atualizaMediaRegioes()
        self.calcBeta()
        
    # =========================================================================
    
    def gerarMA(self):
        """Gera a Matriz de adjacencia do grafo"""

        # criar uma lista vazia
        self.estrutura = []
        for i in range(0, self.nVertices):
            self.estrutura.append([])
            for j in range(0, self.nVertices):
                self.estrutura[i].append(0)

        # inserir as arestas
        for i in self.arestas:
            self.estrutura[i[0]][i[1]] = 1
            self.estrutura[i[1]][i[0]] = 1
                
    def printMA(self):
        for i in self.estrutura:
            for j in i:
                print(j, end=" ")
            print("")

    # =========================================================================
    
    def calcBeta(self):
        bC = bD = bW = 0
        
        for v in self.vertices:
            bC += v["c"]
            bD += v["d"]
            bW += v["w"]
            
        bC /= self.nRegioes
        bD /= self.nRegioes
        bW /= self.nRegioes
        
        self.betaC = bC
        self.betaD = bD
        self.betaW = bW

    # =========================================================================
    
    def vizinhos(self, v):
        vizinhos = []
        for i in range(self.nVertices):
            if(self.estrutura[v][i]):
                vizinhos.append(i)
        
        return vizinhos 
    
    # =========================================================================
            
    def printVertices(self):
        for i in self.vertices:
            print("id:{:3}, regiao:{:2}, c:{:4}, d:{:4}, w:{:4}, x:{: 2.4f}, y:{: 2.4f}".format(
                i["id"], i["regiao"], i["c"], i["d"], i["w"], i["x"], i["y"]))
                
    # =========================================================================
    '''
    def printRegioes(self):
        for k in self.regioes:
            print("{:3}: {}".format(k, self.regioes[k]))
    '''        
   # =========================================================================
    '''
    def vizinhosRegiao(self, r):
        vizinhos = []

        for v in self.regioes[r]:
            vizinhos += self.vizinhos(v)

        return list(set(vizinhos))
    '''
    # =========================================================================
    
    def atualizaMediasRegiao(self, r):
        mC = mD = mW = 0
        
        for i in self.regioes[r]["vertices"]:
            v = self.vertices[i]
            mC += v["c"]
            mD += v["d"]
            mW += v["w"]
        
        n = len(self.regioes[r])
        mC /= n
        mD /= n
        mW /= mW
        
        return ({"mC":mC, "mC":mD, "mW":mW})
    
    def atualizaMediaRegioes(self):
        for i in 
        
    
    # =========================================================================
    
    '''
    def calcRegioes(self):
        # Calcular numero de regioes
        nRegioes = len(self.regioes.keys())
        
        # Acessar cada região
        for r in self.regioes:
            print("r: {}".format(r))
            vizinhos = self.vizinhosRegiao(r)
            print("vizinhos: {}".format(vizinhos))
            
            # Para cada viznho
            for v in vizinhos:
                print("\tv: {}".format(v))
            
            print("")
    '''
